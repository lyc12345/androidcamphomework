package qaq.homework;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


/**
 * Created by lyc12345 on 2016/8/20.
 */

public class NetworkReceiver extends BroadcastReceiver {

    public static boolean wasConnected = true;

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetInfo != null) {
            if(!(context instanceof MainActivity)){
                return;
            }
            if(!wasConnected) {
                Intent intent1 = new Intent(context,SendPostService.class);
                intent1.putExtra("clear_queue",true);
                context.startService(intent1);
            }
            wasConnected = true;
        }else {
            wasConnected = false;
        }

    }
}