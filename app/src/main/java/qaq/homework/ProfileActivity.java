package qaq.homework;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by lyc12345 on 2016/8/19.
 */

public class ProfileActivity extends AppCompatActivity {
    Button okBtn;
    EditText lastnameET,firstnameET,emailET,phoneET,nickET;
    String lastname,firstname,email,phone,nick;
    @Override
    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile);
        setTitle("Edit Account");
        Bundle bundle = getIntent().getExtras();
        lastname = bundle.getString("lastname");
        firstname = bundle.getString("firstname");
        email = bundle.getString("email");
        phone = bundle.getString("phone");
        nick = bundle.getString("nick");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        lastnameET = (EditText) findViewById(R.id.lastname);
        firstnameET = (EditText) findViewById(R.id.firstname);
        emailET = (EditText) findViewById(R.id.email);
        phoneET = (EditText) findViewById(R.id.phone);
        nickET = (EditText) findViewById(R.id.nick);
        okBtn = (Button)findViewById(R.id.sure);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("lastname",lastnameET.getText().toString());
                resultIntent.putExtra("firstname",firstnameET.getText().toString());
                resultIntent.putExtra("email",emailET.getText().toString());
                resultIntent.putExtra("phone",phoneET.getText().toString());
                resultIntent.putExtra("nick",nickET.getText().toString());
                setResult(RESULT_OK,resultIntent);
                finish();
            }
        });
        if(lastname!=null) lastnameET.setText(lastname);
        if(firstname!=null) firstnameET.setText(firstname);
        if(email!=null) emailET.setText(email);
        if(phone!=null) phoneET.setText(phone);
        if(nick!=null) nickET.setText(nick);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
