package qaq.homework;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by lyc12345 on 2016/8/20.
 */

public class MyAdapter extends BaseAdapter{
    LayoutInflater layoutInflater;
    ArrayList<Article>list;
    public MyAdapter(Context context,ArrayList<Article> _list){
        layoutInflater = LayoutInflater.from(context);
        list = _list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View v, ViewGroup viewGroup)  {
        if(v ==null) v = layoutInflater.inflate(R.layout.list_item_article,null);
        TextView title = (TextView)v.findViewById(R.id.title);
        TextView content = (TextView)v.findViewById(R.id.content);
        TextView name = (TextView)v.findViewById(R.id.name);
        Article article = (Article) getItem(i);
        title.setText(article.getTitle());
        content.setText(article.getContent());
        name.setText("By "+article.getName());
        return v;
    }

    public void updateArrayList(ArrayList<Article> _list){
        list = _list;
        notifyDataSetChanged();;
    }
}
