package qaq.homework;

import android.provider.BaseColumns;

/**
 * Created by lyc12345 on 2016/8/20.
 */

public class ArticleContract {
    private ArticleContract(){}
    public static final class Article implements BaseColumns{
        private Article(){}
        public static final String TABLE_NAME = "article";
        public static final String COLUMN_NAME_NICKNAME = "nickname";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String _ID = "_ID";
    }
}
