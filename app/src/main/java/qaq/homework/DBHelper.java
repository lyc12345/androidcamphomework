package qaq.homework;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lyc12345 on 2016/8/20.
 */

public class DBHelper extends SQLiteOpenHelper{
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "article.db";
    private static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS "+ArticleContract.Article.TABLE_NAME+"("+
                    ArticleContract.Article._ID+" INTEGER PRIMARY KEY,"+
                    ArticleContract.Article.COLUMN_NAME_TITLE+" TEXT,"+
                    ArticleContract.Article.COLUMN_NAME_CONTENT+" TEXT,"+
                    ArticleContract.Article.COLUMN_NAME_NICKNAME+" TEXT"+")";
    private static final String TABLE_DELETE = "DROP TABLE IF EXISTS "+ ArticleContract.Article.TABLE_NAME;
    DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLE_CREATE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(TABLE_DELETE);
        onCreate(sqLiteDatabase);
    }
}
