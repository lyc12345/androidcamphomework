package qaq.homework;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lyc12345 on 2016/8/20.
 */

public class PostActivity extends AppCompatActivity {
    EditText titleET,contentET;
    Button send,later;
    Article article;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_post);
        setTitle("Post");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        titleET = (EditText)findViewById(R.id.title);
        contentET = (EditText)findViewById(R.id.content);
        send = (Button)findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = titleET.getText().toString();
                String content = contentET.getText().toString();
                if(title.equals("")||content.equals("")){
                    Toast.makeText(PostActivity.this,"title and content cannot be empty",Toast.LENGTH_SHORT).show();
                    return;
                }
                String name = getSharedPreferences("userdata",MODE_PRIVATE).getString("nick","Ariel");
                article = new Article(name,title,content);
                if(!hasNetworkConnection()){
                    NoNetworkAlertDialog().show();
                    return;
                }
                Intent intent = new Intent(PostActivity.this,SendPostService.class);
                intent.putExtra("clear_queue",false);
                intent.putExtra("title",title);
                intent.putExtra("content",content);
                intent.putExtra("name",name);
                startService(intent);
                finish();
            }
        });
        later = (Button)findViewById(R.id.later);
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = titleET.getText().toString();
                String content = contentET.getText().toString();
                if(title.equals("")||content.equals("")){
                    Toast.makeText(PostActivity.this,"title and content cannot be empty",Toast.LENGTH_SHORT).show();
                    return;
                }
                String name = getSharedPreferences("userdata",MODE_PRIVATE).getString("nick","Ariel");
                article = new Article(name,title,content);
                if(!hasNetworkConnection()){
                    NoNetworkAlertDialog().show();
                    return;
                }
                SendLaterDialog().show();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public AlertDialog NoNetworkAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("您的手機現在沒有網路，要離線留言嗎？").setCancelable(false)
                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.queue.add(article);
                        dialog.cancel();
                        finish();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );
        return builder.create();
    }

    public AlertDialog SendLaterDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.dialog_sleep,null);
        builder.setView(view);
        final EditText editText = (EditText)view.findViewById(R.id.dialog_et);
        builder.setTitle("定時Po文");
        builder.setCancelable(false)
                .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(PostActivity.this,AlarmReceiver.class);
                        intent.putExtra("clear_queue",false);
                        intent.putExtra("title",article.getTitle());
                        intent.putExtra("content",article.getContent());
                        intent.putExtra("name",article.getName());

                        int t = Integer.parseInt(editText.getText().toString());
                        PendingIntent alarmIntent = PendingIntent.getBroadcast(PostActivity.this, 1,intent, PendingIntent.FLAG_CANCEL_CURRENT);
                        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,SystemClock.elapsedRealtime()+t*1000*60, alarmIntent);
                        dialog.cancel();
                        finish();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        );
        return builder.create();
    }

    private boolean hasNetworkConnection() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

}
