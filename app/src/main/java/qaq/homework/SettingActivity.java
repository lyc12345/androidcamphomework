package qaq.homework;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lyc12345 on 2016/8/19.
 */

public class SettingActivity extends AppCompatActivity {
    static final int EDIT_REQUEST = 0;
    String lastname,firstname,nick;
    String phone,email;
    Button edit;
    SharedPreferences preferences;
    @Override
    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_setting);
        setTitle("Settings");
        preferences = getSharedPreferences("userdata",MODE_PRIVATE);
        lastname = preferences.getString("lastname","Shih");
        firstname = preferences.getString("firstname","Ariel");
        nick = preferences.getString("nick","Ariel");
        phone = preferences.getString("phone","09XXXXXXXX");
        email = preferences.getString("email","a@b.com");
        ((TextView)findViewById(R.id.name)).setText(firstname+" "+lastname);
        ((TextView)findViewById(R.id.nick)).setText(nick);
        ((TextView)findViewById(R.id.email)).setText(email);
        ((TextView)findViewById(R.id.phone)).setText(phone);
        edit = (Button)findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingActivity.this,ProfileActivity.class);
                intent.putExtra("lastname",lastname);
                intent.putExtra("firstname",firstname);
                intent.putExtra("nick",nick);
                intent.putExtra("phone",phone);
                intent.putExtra("email",email);
                startActivityForResult(intent,EDIT_REQUEST);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected  void onActivityResult(int requestCode,int resultCode, Intent data){
        if(requestCode == EDIT_REQUEST){
            if(resultCode == RESULT_OK){
                update(data.getStringExtra("nick"),nick);
                lastname = data.getStringExtra("lastname");
                firstname = data.getStringExtra("firstname");
                email = data.getStringExtra("email");
                phone = data.getStringExtra("phone");
                nick = data.getStringExtra("nick");
                ((TextView)findViewById(R.id.name)).setText(firstname+" "+lastname);
                ((TextView)findViewById(R.id.nick)).setText(nick);
                ((TextView)findViewById(R.id.email)).setText(email);
                ((TextView)findViewById(R.id.phone)).setText(phone);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("lastname",lastname);
                editor.putString("firstname",firstname);
                editor.putString("email",email);
                editor.putString("phone",phone);
                editor.putString("nick",nick);
                editor.commit();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void update(final String newValue, String oldValue){
        Query query = MainActivity.postReference.orderByChild("name").equalTo(oldValue);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot:dataSnapshot.getChildren()){
                    String key = snapshot.getKey();
                    Map<String,String> map = (Map<String, String>) snapshot.getValue();
                    map.put("name", newValue);
                    Map<String,Object> child = new HashMap<>();
                    child.put(key,map);
                    MainActivity.postReference.updateChildren(child);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
