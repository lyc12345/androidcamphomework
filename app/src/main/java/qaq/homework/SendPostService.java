package qaq.homework;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;

import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lyc12345 on 2016/8/21.
 */

public class SendPostService extends IntentService{
    public SendPostService(){
        super("SendPostService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean clearQueue = intent.getBooleanExtra("clear_queue",false);
        if(clearQueue){
            while (!MainActivity.queue.isEmpty()){
                Article article = MainActivity.queue.poll();
                String key = MainActivity.postReference.push().getKey();
                Map<String,String> map = article.toHashMap();
                Map<String,Object> child = new HashMap<>();
                child.put(key,map);
                MainActivity.postReference.updateChildren(child);
                sendDataToInternet(article.getTitle());
            }
        }
        else{
            String name = intent.getStringExtra("name");
            String content = intent.getStringExtra("content");
            String title = intent.getStringExtra("title");
            Article article = new Article(name,title,content);
            String key = MainActivity.postReference.push().getKey();
            Map<String,String> map = article.toHashMap();
            Map<String,Object> child = new HashMap<>();
            child.put(key,map);
            MainActivity.postReference.updateChildren(child);
            sendDataToInternet(title);
        }
    }

    private String sendDataToInternet(String title){
        HttpPost httpRequest = new HttpPost("https://fcm.googleapis.com/fcm/send");
        try
        {
            httpRequest.setHeader("Authorization","key=AIzaSyDMGQySnS9TtEAd4Gbli1KUIYCKsN-hWYU");
            httpRequest.setHeader("Content-Type","application/json");

            String json = "{\"to\":\"/topics/all\",\"data\":{\"body\":\""+title+"\",\"title\":\"New Post \"}}";
            httpRequest.setEntity(new ByteArrayEntity(json.getBytes("UTF8")));
            HttpParams httpParams = new BasicHttpParams();
            httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
            HttpConnectionParams.setSoTimeout(httpParams, 10 * 1000);
            new DefaultHttpClient(httpParams).execute(httpRequest);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return "";
    }


}
