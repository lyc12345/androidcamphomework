package qaq.homework;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public static LinkedList<Article> queue = new LinkedList<>();
    Button btnSetting;
    Button btnPost;
    Button btnRefrsh;
    ListView listView;
    MyAdapter adapter;
    ArrayList<Article>list;
    public static FirebaseDatabase database;
    public static DatabaseReference postReference;
    NetworkReceiver receiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = FirebaseDatabase.getInstance();
        postReference = database.getReference("post");

        //initData();
        btnSetting = (Button) findViewById(R.id.setting);
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,SettingActivity.class);
                startActivity(intent);
            }
        });
        btnRefrsh = (Button) findViewById(R.id.refresh);
        btnRefrsh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadData();
            }
        });
        btnPost = (Button) findViewById(R.id.post);
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,PostActivity.class);
                startActivity(intent);
            }
        });
        list = new ArrayList<>();
        adapter = new MyAdapter(MainActivity.this,list);
        listView = (ListView)findViewById(R.id.listview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this,ArticleActivity.class);
                intent.putExtra("content",list.get(i).getContent());
                intent.putExtra("name",list.get(i).getName());
                intent.putExtra("title",list.get(i).getTitle());
                startActivity(intent);
            }
        });
        FirebaseMessaging.getInstance().subscribeToTopic("all");

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        receiver = new NetworkReceiver();
        registerReceiver(receiver,intentFilter);
        loadData();
    }

    private void initData(){
        String[] users = {"Ariel", "Eric", "Angela"};
        String[] titles = {"test1", "test2", "test3"};
        String[] contents = {"for test1", "for test2", "for test3"};
        for (int i = 0; i < 3; i++) {
            Article article = new Article(users[i],titles[i],contents[i]);
            String key = postReference.push().getKey();
            Map<String,String> map = article.toHashMap();
            Map<String,Object> child = new HashMap<>();
            child.put(key,map);
            postReference.updateChildren(child);
        }
    }

    private void loadData(){
        list = new ArrayList<>();
        findViewById(R.id.progressbar).setVisibility(View.VISIBLE);
        postReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int size = (int)dataSnapshot.getChildrenCount();
                Iterator it = dataSnapshot.getChildren().iterator();
                while(it.hasNext()){
                    HashMap<String,Object> map = (HashMap<String,Object>) ((DataSnapshot)(it.next())).getValue();
                    Article article = new Article((String)map.get("name"),(String)map.get("title"),(String)map.get("content"));
                    list.add(article);
                }
                adapter.updateArrayList(list);
                findViewById(R.id.progressbar).setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    @Override
    protected void onDestroy(){
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
