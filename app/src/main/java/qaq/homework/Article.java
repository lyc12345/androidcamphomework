package qaq.homework;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lyc12345 on 2016/8/20.
 */

public class Article {
    String name;
    String title;
    String content;
    public Article(String _name,String _title,String _content){
        name = _name;
        title = _title;
        content = _content;
    }
    public String getName(){return name;}
    public String getTitle(){return title;}
    public String getContent(){return content;}
    public Map<String,String> toHashMap(){
        HashMap<String,String> map = new HashMap<>();
        map.put("name",name);
        map.put("title",title);
        map.put("content",content);
        return map;
    }
}
