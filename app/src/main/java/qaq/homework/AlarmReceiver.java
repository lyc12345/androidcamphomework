package qaq.homework;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by lyc12345 on 2016/8/21.
 */

public class AlarmReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Intent intent1 = new Intent(context,SendPostService.class);
        intent1.putExtras(bundle);
        context.startService(intent1);
    }
}
